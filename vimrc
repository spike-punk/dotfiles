filetype plugin indent on

"set showcmd		" Show (partial) command in status line.
set showmatch
set noerrorbells

" Files & History
set noswapfile
set nobackup
set undodir=~/.vim/undo
set undofile

" visual stuff
syntax on
set number
set linebreak
set showbreak=+++
set hlsearch
set showmatch
set nocompatible
set termguicolors
"" Cursorline
set cursorline
set cursorcolumn
"" Fix Splitting
set splitbelow splitright

" Language specific things
set spelllang=de,en
set encoding=utf-8
autocmd BufNewFile,BufRead * :set spell
autocmd BufRead,BufNewFile *.py,*.java,*.sh,*.c,*.css,*.php,*js :set nospell "no autocorrect in code

" my tabs are 4 spaces long
set expandtab
set tabstop=4
set shiftwidth=4
set smartindent

" I don't use my mouse in terminal
set mouse=
set ttymouse=

" vim-plug plugins
call plug#begin('/usr/share/vim/plugins')
" Organize
Plug 'vimwiki/vimwiki'
Plug 'itchyny/calendar.vim'
Plug 'ledger/vim-ledger'
Plug 'dhruvasagar/vim-table-mode'

" visual appereance
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tomasiser/vim-code-dark'
Plug 'chrisbra/unicode.vim'

" git
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'

" Coding
Plug 'tpope/vim-commentary'
Plug 'godlygeek/tabular'
Plug 'mikelue/vim-maven-plugin'
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Pandoc integration
Plug 'vim-pandoc/vim-pandoc'
"Plug 'vim-pandoc/vim-pandoc-syntax'
Plug 'sotte/presenting.vim'

" smaller useful stuff
Plug 'tpope/vim-surround'
Plug 'robertbasic/vim-hugo-helper'

" Hugo
Plug 'robertbasic/vim-hugo-helper'

call plug#end()
let g:vimwiki_list=[{'auto_diary_index': 1, 'path':'~/vimwiki', 'syntax':'default', 'ext':'.wiki'}]
let g:vimwiki_global_ext = 0
colorscheme codedark

" search
set incsearch
set path+=*
set wildmenu
set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc
set ignorecase
set smartcase

" Autocomplete
set wildmode=longest,list,full

" tagging
command! MakeTags !ctags -R .

" X clipboard
" put selected to clipboard
vnoremap \yy :'<,'> !xclip -selection clipboard<CR>u
" put clipboard content in file
nnoremap \vv :r !xclip -o<CR>i<BS>

" visual block indent
vmap < <gv
vmap > >gv
" auto indent
map <leader>i :setlocal autoindent<CR>
map <leader>I :setlocal noautoindent<CR>

" moving in split vim
map <A-h> <C-w>h
map <A-j> <C-w>j
map <A-k> <C-w>k
map <A-l> <C-w>l

" DOCUMENT SPECIFIC SETTINGS
" ---- Markdown ---- 
autocmd BufRead,BufNewFile *.md,*.markdown map <leader>c :!pandoc -H /usr/share/vim/add/md-header.tex -o %:r.pdf %

" ---- LaTex Settings ----
let g:tex_flavor='latex'
let g:Tex_DefaultTargetFormat='pdf'

" ---- COC Settings ----
let g:coc_data_home = '/usr/share/vim/addons/vim-coc'
autocmd BufRead,BufNewFile *.ledger let b:coc_additional_keywords =  [":"]
autocmd BufRead,BufNewFile * :CocDisable
autocmd BufRead,BufNewFile *.py,*.java,*.html,*.htm,*.c,*.css,*.php,*.js,*.ledger :CocEnable

