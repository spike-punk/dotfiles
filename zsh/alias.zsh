alias s="sudo"
alias l="ls -l --color=always --group-directories-first"
alias vi=vim
alias svim="sudo vim"
alias cp="cp -iv"
alias mv="mv -iv"
alias rm="rm -rvI"
alias mkdir="mkdir -p"
alias grep="grep --color=auto"
alias diff="diff --color=auto"
alias e="vim"
alias dict="sdcv --utf8-output --utf8-input -n --data-dir=/usr/share/stardict/dict/"
alias shutdown="sudo shutdown now"

# git aliases 
alias g="git"
alias ga="git add"
alias gap="git add --patch"
alias gc="git commit -v"
alias gcsm="git clone --recurse-submodules"
alias gd="git diff"
alias gps="git push"
alias gpl="git pull"

# debain spec. aliases
alias apti="sudo apt install"
alias aptu="sudo apt update && sudo apt upgrade"
alias aptp="sudo apt purge"
alias aptc="sudo apt autoremove"
alias apts="apt search"
alias aptac="sudo apt autoclean" 

alias cps="l /usr/share/templates/"

# Qubes OS aliases
alias gpg="qubes-gpg-client-wrapper"
alias qmv="qvm-move"
alias qcp="qvm-copy"
alias qpdf="qvm-convert-pdf"
function qimg {
    qvm-convert-img $@ $@.trusted
}
alias qopen="qvm-open-in-dvm"

function cpt {
    cp /usr/share/templates/$@ .
}

alias khal="vdirsyncer sync && khal interactive"
